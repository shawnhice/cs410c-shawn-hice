from flask import render_template
from flask.views import MethodView
import gbmodel

class Index(MethodView):
    def get(self):
        model = gbmodel.get_model()
        entries = [dict(name=row[0], eggs=row[1], milk=row[2], flour=row[3], added=row[4], additional=row[5]) for row in model.select()]
        return render_template('index.html',entries=entries)
