# Imports the Google Cloud client library
from google.cloud import vision
from google.cloud.vision import types

# This gets called in index.py, accesses the google vision library
def getLabels(url):
    # Instantiates a google vision client
    client = vision.ImageAnnotatorClient()

    # Performs label detection on the image file, can also perform face detection etc
    response = client.annotate_image({
     'image': {'source': {'image_uri': url}},
      'features': [{'type': vision.enums.Feature.Type.LABEL_DETECTION}],
    })
    
    # Returns list with Description, Score, etc.
    return response.label_annotations