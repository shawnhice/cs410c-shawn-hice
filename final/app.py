"""
Flask app that generates a web hosted way to interact with the Google Vision API
"""
import flask
from flask.views import MethodView
from index import Index

app = flask.Flask(__name__)       # Flask app

# This is a single page flask application
app.add_url_rule('/',
                 view_func=Index.as_view('index'),
                 methods=['GET', 'POST'])

# Runs on localhost port=80 
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True)
