class Model():
    def select(self):
        """
        Gets all entries from the database
        :return: Tuple containing all rows of database
        """
        pass

    def insert(self, url, labels):
        """
        Appends a new list of values representing new image into visionlist
        :param url: String
        :param labels: List
        :return: True
        """
        pass
