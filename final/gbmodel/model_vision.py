"""
Python list model for google vision object
"""
from datetime import date
from .Model import Model

class model(Model):
    def __init__(self):
        self.labels_entries = []

    def select(self):
        """
        Returns visionlist list of lists
        Each list in visionlist contains: URL, Labels {Description, Score, additional}
        :return: List of lists
        """
        return self.labels_entries

    def insert(self, url, labels):
        """
        Appends a new list of values representing new image into visionlist
        :param url: String
        :param labels: List
        :return: True
        """
        # Make a params array and then append the data
        params = []
        params.append(url)
        params.append(labels)
        params.append(date.today())

        # Add params to class member variable
        self.labels_entries.append(params)

        #return True
        return True
