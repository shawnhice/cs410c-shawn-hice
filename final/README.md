# Google Vision API Demo and Final

Created by: **Shawn Hice**

08/13/2019

## Setup

1. Create a user profile for accessing the google cloud api [tutorial]{https://cloud.google.com/docs/authentication/production#auth-cloud-implicit-python}

2. Generate a key for the user profile and download the json file associated with the key

3. Add a Google Vision API key to your local env variables ` export GOOGLE_APPLICATION_CREDENTIALS=[key] `

#### You must repeat step 3 if you close and re-open your terminal on unix 

#### Windows must follow a slightly different guide for step 3

## Building

1. Ensure you have python and pip installed ` apt install python3-pip ` 

2. Ensure you have virtualenv installed ` pip3 install virtualenv `

3. Add your virtualenv once in the final directory ` virtualenv -p Python3 env `

4. Add your source directory ` source env/bin/activate `

5. Download required modules via pip ` pip3 -r requirements.txt `

5. Run the python application ` python3 app.py `

6. Go to [localhost:5000]{http://localhost:5000}

## Testing

#### There are no built in tests unfortunetly

1. Go to [localhost:5000]{http://localhost:5000} or http://cs410c-shawn-hice.appspot.com/

2. ` https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiEvPHduIDkAhX3GTQIHVJIBYcQjRx6BAgBEAQ&url=https%3A%2F%2Fwww.pexels.com%2Fsearch%2Fflower%2F&psig=AOvVaw0QTDLpec8UGxV1NG-H_wYN&ust=1565806186093462 `

### Errors

 - Google Cloud Vision API access error see **Setup**