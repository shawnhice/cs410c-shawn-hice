from flask import render_template
from flask import redirect, request, url_for
from flask.views import MethodView
import gbmodel
import vision

class Index(MethodView):
    def get(self):
        """
        Accepts GET requests, and retrieves the data;
        Renders when complete.
        Gets called when refresh happens in order to display the list data.
        """

        # Retrieves model that is URL, Labels, and Date
        model = gbmodel.get_model()
        labels = [dict(URL=row[0], labels=row[1], date=row[2]) for row in model.select()]
        
        # Re-Renders
        return render_template('index.html',labels=labels)

    def post(self):
        """
        Accepts POST requests, and processes the form;
        Redirect to index when completed. 
        This it to refresh the page to diplay the new data associated with the list.
        """
        # Custom Vision class from vision.py
        labels = vision.getLabels(request.form['URL'])

        # Adds list of URL, labels, Date 
        model = gbmodel.get_model()
        model.insert(request.form['URL'], labels)
        
        # Re-Renders Viewer (calls get)
        return redirect(url_for('index'))