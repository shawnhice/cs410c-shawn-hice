class Model():
    def select(self):
        """
        Gets all entries from the database
        :return: Tuple containing all rows of database
        """
        pass

    def insert(self, name, eggs, milk, flour, additional):
        """
        Inserts entry into database
        :param name: String
        :param eggs: Number
        :param milk: Number
        :param flour: Number
        :param additional: String
        :return Done
        :raises: Database errors on connection and insertion
        """
        pass
