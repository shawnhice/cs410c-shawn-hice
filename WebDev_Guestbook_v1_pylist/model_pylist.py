"""
Data is stored in a Python list.  Returns a list of lists
  upon retrieval
"""
from datetime import date
from Model import Model

class model(Model):
    def __init__(self):
        self.guestentries = []

    def select(self):
        """
        Returns guestentries Dictionary
        Each Dictionary item in guestentries contains name, email, signed_on, messgae
        :return: Array of Dictionaries
        """
        return self.guestentries

    def insert(self, name, email, message):
        """
        Appends a new Dictionary of values representing new message into guestentries
        :param name: String
        :param email: String
        :param message: String
        :return: True
        """
        item = dict(name=name, email=email, signed_on=date.today(), message=message )
        self.guestentries.append(item)
        return True
