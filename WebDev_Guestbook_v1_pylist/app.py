"""
A simple guestbook flask app.
"""
from flask import Flask, redirect, request, url_for, render_template

#from model_sqlite3 import model
from model_pylist import model

app = Flask(__name__)       # our Flask app
model = model()

"""
Function decorator === app.route('/',index())
"""
@app.route('/')
@app.route('/index.html')
def index():
    """
    Dictionary guestbook
    """
    return render_template('index.html', entries=model.select())

@app.route('/sign', methods=['POST'])
def sign():
    """
    Accepts POST requests, and processes the form;
    Redirect to index when completed.
    """
    model.insert(request.form['name'], request.form['email'], request.form['message'])
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
