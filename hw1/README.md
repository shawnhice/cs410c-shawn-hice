# Markdown is pretty neat

## Lots of different kind of things can be in Markdown

` code is pretty important `

### Bullets are equally as cool

- You have a couple of options
..- Eh maybe not so much

#### Numbers are just fancy bullets

1. Make Money
2. Spend Money

### Links have their place in the modern GIT workflow

- Links like this [Oi](https://google.com)

#### Code references can show up in a pretty cool way

https://bitbucket.org/shawnhice/cs410c-shawn-hice/src/eef21a2bd5807dcc8b6a8aeebe4904c84e26450e/.gitignore#lines-3

#### CI references can look pretty cool

[![Build Status](https://travis-ci.org/HiceS/InventorRobotExporter.svg?branch=master)](https://travis-ci.org/HiceS/InventorRobotExporter)

## However nothing beats ascii table integration with DOD JIRA (closed)

|    DOD    | Done |    Reviewer    | Issue | Notes |
|-----------|------|----------------|-------|-------|
| Code      | ❌    | Shawn Hice |   976|       |
| Unit Test | N/A  | N/A            |       |       |
| Merged    | ❌    |                |       |       |