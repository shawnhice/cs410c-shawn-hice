"""
Python list model
"""
from datetime import date
from .Model import Model

class model(Model):
    def __init__(self):
        self.guestentries = []

    def select(self):
        """
        Returns guestentries list of lists
        Each list in guestentries contains: name, eggs, milk, flour, additional
        :return: List of lists
        """
        return self.guestentries

    def insert(self, name, eggs, milk, flour, additional):
        """
        Appends a new list of values representing new message into guestentries
        :param name: String
        :param eggs: Number
        :param milk: Number
        :param flour: Number
        :param additional: String
        :return: True
        """
        params = [name, eggs, milk, flour, date.today(), additional]
        self.guestentries.append(params)
        return True
