"""
A simple guestbook flask app.
ata is stored in a SQLite database that looks something like the following:

+------------+------------------+------------+----------------+
| Name       | Email            | signed_on  | message        |
+============+==================+============+----------------+
| John Doe   | jdoe@example.com | 2012-05-28 | Hello world    |
+------------+------------------+------------+----------------+

This can be created with the following SQL (see bottom of this file):

    create table guestbook (name text, eggs text, milk text, flour text, signed_on date, additional);

"""
from datetime import date
from .Model import Model
import sqlite3
DB_FILE = 'entries.db'    # file for our Database

class model(Model):
    def __init__(self):
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from recipies")
        except sqlite3.OperationalError:
            cursor.execute("create table recipies (name text, eggs text, milk text, flour text, signed_on date, additional)")
        cursor.close()

    def select(self):
        """
        Gets all rows from the database
        Each row contains: name, eggs, milk, flour, date, additional
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM recipies")
        return cursor.fetchall()

    def insert(self, name, eggs, milk, flour, additional):
        """
        Inserts entry into database
        :param name: String
        :param eggs: Number
        :param milk: Number
        :param flour: Number
        :param additional: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = {'name':name, 'eggs':eggs, 'milk':milk, 'flour':flour, 'date':date.today(), 'additional':additional}
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert into recipies (name, eggs, milk, flour, signed_on, additional) VALUES (:name, :eggs, :milk, :flour, :date, :additional)", params)

        connection.commit()
        cursor.close()
        return True
